public class Lion extends Animal {

  public Lion(String name, String generation, int age, Stable stable) {
    super(name, generation, age, stable);
  }

  public String toString() {
    return "Hi my name is " + getName() +
            "\nI'm : " + getGeneration() +
            "\nAnd I am " + getAge();
  }
}
