public class TestAnimal {

  public static void main(String[] args) {
    Goat goat = new Goat(
            "Guri",
            "Unclear info",
            3,
            new Food("Ben and Jerry"),
            new Stable(
            "Ekeberg parken",
            "Horsebox Nr. 5",
            "EKT"));
    Lion lion = new Lion("Simba", "Unclear info", 1, null);

    System.out.println(goat.toString());
    System.out.println();
    System.out.println(lion.toString());

  }

}
