public class Goat extends Animal implements Feedable {

  private Food favouriteFood;

  public Goat(String name, String generation, int age, Food favouriteFood, Stable stable) {
    super(name, generation, age, stable);
    setFavouriteFood(favouriteFood);
  }

  public Food getFavouriteFood() {
    return favouriteFood;
  }

  public void setFavouriteFood(Food favouriteFood) {
    this.favouriteFood = favouriteFood;
  }

  public String toString() {
    return "My name is " + getName() +
            "\nI am generation: " + getGeneration() +
            "\nAnd I am : " + getAge() +
            "\nI'm pleased if you feed me my favoite food: " + getFavouriteFood().toString();
  }

  public String feedAnimal(Food food) {
    if(food == getFavouriteFood()){
      return "Omnomnom";
    } else {
      return "Yuck";
    }
  }
}
