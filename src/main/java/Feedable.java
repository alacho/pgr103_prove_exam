public interface Feedable {

  String feedAnimal(Food food);
}
