public abstract class Animal {

  private String name, generation;
  private int age;
  private Stable stable;

  public Animal(String name, String generation, int age, Stable stable) {
    this.name = name;
    this.generation = generation;
    this.age = age;
    this.stable = stable;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getGeneration() {
    return generation;
  }

  public void setGeneration(String generation) {
    this.generation = generation;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public Stable getStable() {
    return stable;
  }

  public void setStable(Stable stable) {
    this.stable = stable;
  }

  @Override
  public abstract String toString();
}
