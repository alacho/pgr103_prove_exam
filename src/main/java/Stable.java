public class Stable {
  private String location, box, farm;

  public Stable(String location, String box, String farm) {
    this.location = location;
    this.box = box;
    this.farm = farm;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getBox() {
    return box;
  }

  public void setBox(String box) {
    this.box = box;
  }

  public String getFarm() {
    return farm;
  }

  public void setFarm(String farm) {
    this.farm = farm;
  }

  @Override
  public String toString() {
    return "Location:" + getLocation() +
            "Box: " + getBox() +
            "Farm" + getFarm();
  }
}
